# -*- coding: utf-8 -*-
__author__ = 'paul'


# catch university

import re, json, codecs, requests
import utils


# university
intro_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>学校简介')
major_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>开设专业')
score_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>历年分数线')
register_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>报考指南')
plan_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>招生计划')
tutor_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>师资力量')
figure_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>人气校友')
comment_pat = re.compile('<ahref="(?P<link>[^"]*)"><strong>学校评论')

# 1. 学校简介
info_pat = re.compile(utils.clean("""
        <ul class="baseInfo_left">
            <li class="biItem"> <span class="t">创建时间</span>
              <div class="c">(?P<创建时间>.*?)</div>
            </li>
            <li class="biItem"> <span class="t">隶属于</span>
              <div class="c">(?P<隶属于>.*?)</div>
            </li>
            <li class="biItem"> <span class="t">学生人数</span>
              <div class="c">(?P<学生人数>.*?)</div>
            </li>
            <li class="biItem"> <span class="t">院士人数</span>
              <div class="c">(?P<院士人数>.*?)</div>
            </li>
        </ul>
        <ul class="baseInfo_right">
            <li class="biItem"> <span class="t">重点学科</span>
              <div class="c">(?P<重点学科>.*?)</div>
            </li>
            <li class="biItem"> <span class="t">学校类型</span>
              <div class="c">(?P<学校类型>.*?)</div>
            </li>
            <li class="biItem"> <span class="t">博士点个数</span>
              <div class="c">(?P<博士点个数>.*?)</div>
            </li>
            <li class="biItem"> <span class="t">硕士点个数</span>
              <div class="c">(?P<硕士点个数>.*?)</div>
            </li>
        </ul>"""))
profession_pat = re.compile('就业情况</h2></div><divclass="txt"style="(.*?)">(?P<text>.*?)</div>')
hometown_pat = re.compile('data:\[(?P<hometown>[.,"\[\]0-9东南西北华]+)\]')
male_pat = re.compile('男生<br/>(?P<percent>[.0-9]+)%')
female_pat = re.compile('女生<br/>(?P<percent>[.0-9]+)%')
special_major_pat = re.compile(
    '<divclass="boxtxt"style="display:block">(?P<majors>(<h3>.*?</h3><p>.*?</p>)+)</div>')
special_major_sub_pat = re.compile('<h3>(?P<name>.*?)</h3><p>(?P<intro>.*?)</p>')
critical_major_pat = re.compile('<divclass="boxtxt">(?P<majors>.*?)</div>')
critical_major_sub_pat = re.compile(
    '<h3>(?P<type>.*?)</h3><ulclass="listclearfix">(?P<list>.*?)</ul>')
critical_major_sub_sub_pat = re.compile('<li><ahref="(?P<link>.*?)">(?P<name>.*?)</a></li>')
fee_pat = re.compile('学费信息</h2></div><divclass="txt"style="text-indent:28px;">(?P<info>.*?)</div>')


def collect_intro(link):
    page = utils.catch_html(link)
    data = {}
    data['基本信息'] = info_pat.search(page).groupdict()
    data['就业情况'] = profession_pat.search(page).group('text')
    data['男女比例'] = {
        '男': male_pat.search(page).group('percent'),
        '女': female_pat.search(page).group('percent')
    }

    hometowns = json.loads('[' + hometown_pat.search(page).group('hometown') + ']')
    data['学生来源'] = {}
    for hometown in hometowns:
        data['学生来源'][hometown[0]] = hometown[1]

    majors = special_major_pat.search(page).group('majors')
    data['特色专业'] = []
    for major in special_major_sub_pat.finditer(majors):
        data['特色专业'].append({
            '名称': major.group('name'),
            '介绍': major.group('intro')
        })

    major_list = critical_major_pat.search(page).group('majors')
    data['重点专业'] = []
    for major_type in critical_major_sub_pat.finditer(major_list):
        m = {}
        m['分类'] = major_type.group('type')
        m['专业'] = []
        for major in critical_major_sub_sub_pat.finditer(major_type.group('list')):
            m['专业'].append({
                '名称': major.group('name'),
                '链接': major.group('link')
            })
        data['重点专业'].append(m)

    data['学费信息'] = fee_pat.search(page).group('info')

    return data


# 2. 开设专业
majors_pat = re.compile(
    '<divclass="majorCon"><h3>■(?P<category>.*?)（(?P<count>\d+)个）</h3><ulclass="listclearfix">(?P<list>.*?)</ul></div>')
majors_has_link_pat = re.compile('<li><ahref="(?P<link>.*?)">(?P<name>.*?)</a></li>')
majors_no_link_pat = re.compile('<listyle=".*?">(?P<name>.*?)</li>')
majors_intro_pat = re.compile(
    '<divclass="catTitle"><h2>.*?<spanstyle=".*?"><a.*?>.*?</a></span></h2></div><divclass="txt">(?P<intro>.*?)</div>')


def collect_major(link):
    page = utils.catch_html(link)
    data = []
    for category in majors_pat.finditer(page):
        c = {}
        c['分类'] = category.group('category')
        c['个数'] = int(category.group('count'))
        c['专业'] = []
        for major in majors_has_link_pat.finditer(category.group('list')):
            the_page = utils.catch_html(major.group('link'))
            c['专业'].append({
                '名称': major.group('name'),
                '链接': major.group('link'),
                '介绍': majors_intro_pat.search(the_page).group('intro')
            })
        for major in majors_no_link_pat.finditer(category.group('list')):
            c['专业'].append({
                '名称': major.group('name'),
                '链接': None,
                '介绍': None
            })
        data.append(c)

    return data


score_table_pat = re.compile('<table.*?>(?P<table>.*?)</table>')


# 3. 分数线
def collect_score(link):
    page = utils.catch_html(link)
    data = {}
    idx = 0
    for table in score_table_pat.finditer(page):
        if idx == 0:
            data['历年分数线'] = utils.parse_table(table.group('table'))
        elif idx == 1:
            data['专业分数线'] = utils.parse_table(table.group('table'))
        idx += 1

    return data


# 4. 报考指南
guide_pat = re.compile('报考指南</h2></div><divclass="txt">(?P<text>.*?)</div>')


def collect_register(link):
    page = utils.catch_html(link)
    return guide_pat.search(page).group('text')


# 5. 招生计划
subject_pat = re.compile(
    '<lidata-val.*?onclick="\$\.setVar\(\'claimSubType\',(?P<id>\d+)\)">(?P<text>.*?)</li>')
city_pat = re.compile(
    '<lidata-val.*?onclick="\$\.setVar\(\'claimCity\',(?P<id>\d+)\)">(?P<text>.*?)</li>')


def collect_plan(link):
    uid = utils.get_uid_from_url(link)
    page = utils.catch_html(link)
    subjects = []
    for subject in subject_pat.finditer(page):
        subjects.append(subject.groupdict())
    cities = []
    for city in city_pat.finditer(page):
        cities.append(city.groupdict())

    data = []
    for subject in subjects:
        for city in cities:
            url = 'http://www.gaokaopai.com/university-ajaxGetMajor.html'
            form = {
                'id': uid,
                'type': subject['id'],
                'city': city['id'],
                'state': 1
            }
            # IMPORTANT!
            headers = {
                'X-Requested-With': 'XMLHttpRequest'
            }
            json_data = requests.post(url, form, headers=headers).json()
            if json_data['status'] == 1:  # success
                data.append({
                    '类型': subject['text'],
                    '地区': city['text'],
                    '计划': utils.list_map(lambda e: {
                        '专业名称': e['profess'],
                        '计划类型': e['type'],
                        '层次': e['level'],
                        '科类': e['subject'],
                        '学制': e['stu_time'],
                        '计划数': e['plan']
                    }, json_data['data'])
                })

    return data


# 6. 师资力量
ins_main_pat = re.compile('<ulclass="listclearfix">(?P<list>.*?)</ul>')
ins_pat = re.compile(
    '<li><ahref="(?P<link>.*?)">(?P<name>.*?)</a><spanclass="tea">\((?P<count>\d+)\)</span></li>')
tutor_name_pat = re.compile('<lititle="(?P<title>.*?)">')


def collect_tutor(link):
    page = ins_main_pat.search(utils.catch_html(link)).group('list')
    data = []
    for ins in ins_pat.finditer(page):
        l = len(ins.group('name'))
        the_page = utils.catch_html(ins.group('link'))
        data.append({
            '院系名称': ins.group('name'),
            '链接': ins.group('link'),
            '教师数量': ins.group('count'),
            '教师姓名': utils.list_map(lambda e: e.group('title')[l:], tutor_name_pat.finditer(the_page))
        })

    return data


# 7. 人气校友
people_pat = re.compile('<li><ahref=".*?"><h3>(?P<name>.*?)</h3></a><p>(?P<intro>.*?)</p></li>')


def collect_figure(link):
    page = utils.catch_html(link)
    data = []
    for people in people_pat.finditer(page):
        data.append({
            '姓名': people.group('name'),
            '介绍': people.group('intro')
        })

    return data


# 0. 爬取大学数据
def collect_university(university):
    page = utils.catch_html(university['链接'])
    university['学校简介'] = collect_intro(intro_pat.search(page).group('link'))
    university['开设专业'] = collect_major(major_pat.search(page).group('link'))
    university['分数线'] = collect_score(score_pat.search(page).group('link'))
    university['报考指南'] = collect_register(register_pat.search(page).group('link'))
    university['招生计划'] = collect_plan(plan_pat.search(page).group('link'))
    ret = tutor_pat.search(page)
    if ret:
        university['师资力量'] = collect_tutor(ret.group('link'))
    ret = figure_pat.search(page)
    if ret:
        university['人气校友'] = collect_figure(ret.group('link').replace('xiaohua', 'xiaoyou'))


upat = re.compile(
    '<divclass="tit"><h3><ahref="(?P<link>.*?)">(?P<name>.*?)</a></h3>')


def collect_universities(index):
    index_page = utils.catch_html('http://www.gaokaopai.com/daxue-0-0-0-0-0-0-0--p-%i.html' % index)

    all_universites = []
    for match in upat.finditer(index_page):
        university = {
            '名称': match.group('name'),
            '链接': match.group('link')
        }
        print('catching ' + match.group('name') + ': ' + match.group('link'))
        collect_university(university)
        all_universites.append(university)

    json.dump(all_universites, open('uni_%i.json' % index, 'w'), ensure_ascii=False)


if __name__ == '__main__':
    for i in range(1, 2):
        collect_universities(i)
