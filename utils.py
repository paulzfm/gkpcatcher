__author__ = 'paul'

from urllib.request import urlopen, Request
import re

from bs4 import BeautifulSoup


def clean(text: str) -> str:
    return re.sub(r'\s', r'', text)


def catch_html(url: str, cleaned=True) -> str:
    user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
    headers = {'User-Agent': user_agent}
    req = Request(url, headers=headers)
    response = urlopen(req)
    html = response.read().decode('utf-8')

    if cleaned:
        return clean(html)
    return html


def parse_table(html: str) -> [{str: str}]:
    headers = []
    rows = []
    soup = BeautifulSoup(html, 'html.parser')
    for row in soup.find_all('tr'):
        if len(row.find_all('th')) > 0:
            headers = list_map(lambda x: x.get_text(), row.find_all('th'))
        else:
            rows.append(list_map(lambda x: x.get_text(), row.find_all('td')))

    data = []
    for row in rows:
        d = {}
        for i in range(len(headers)):
            d[headers[i]] = row[i]
        data.append(d)

    return data


id_pat = re.compile('(?P<id>\d+)')


def get_uid_from_url(url: str) -> str:
    return id_pat.search(url).group('id')


# (a -> b) -> [a] -> [b]
def list_map(func: (object, object), lst: [object]) -> [object]:
    return list(map(func, lst))
